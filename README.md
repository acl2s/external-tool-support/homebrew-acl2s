# The ACL2s Tap

## How do I install these formulae?

`brew tap nufm/acl2s https://gitlab.com/acl2s/external-tool-support/homebrew-acl2s.git` and then `brew install acl2s`.

## Updating ACL2 or SBCL

To update ACL2 or SBCL, one needs to update the recipe in this repo
(`Formula/acl2s.rb`). After that, one should rebuild bottles
(described later). When updating the recipe, one will almost always
want to comment out or delete the `bottle do ... end` lines until
bottles have been rebuilt.

To update ACL2, one should change the commit hash in the `url` line to
`https://api.github.com/repos/acl2/acl2/zipball/<commit>` (replacing
`<commit>` with the desired commit), and update the `sha256` value to
be the SHA256-sum of the zipball of the relevant commit. The lazy way
to do this is to update the formula with the new `url` value but don't
update the `sha256` value, copy the formula to your local Homebrew tap
directory, and try to build the recipe. It will fail quickly and give
an error saying that the checksum of that file does not match, and
will output the new checksum.

To update SBCL, one should change the `url` in the `sbcl_files`
resource section to point to an archive of the updated source files,
and change the `sha256` value in a similar way to what is described above.

Updating the ACL2s scripts version can be done in a similar way to
updating SBCL files, instead changing the `url` and `sha256` values in
the `acl2s_scripts` resource section.

## Rebuilding Bottles

After making any needed updates to the recipe and pushing it, one must
rebuild bottles (prebuilt executables) for each combination of OS and
architecture that one wants to support. As of right now, we want to
build bottles for:
- x86_64 Linux
- arm64 (aka M1) macOS
- x86_64 macOS
Note that bottles built for macOS on macOS version `z` will function
on macOS versions greater than or equal to `z`. Therefore, it may be
desirable to build a bottle on a version of macOS one or two versions
behind the latest, so that more versions of macOS can be supported by
a single bottle. However, there may be compatibility issues when a
bottle is built on an older version of macOS than it is run on. See
the Homebrew documentation for more information.

For our purposes, a bottle for a particular OS and architecture
combination can only be built on a machine running that OS and
architecture.

To build a bottle, the following steps should be taken:
- Add the tap if it's not already added (see top of this README)
- `brew update`
- `brew remove acl2s` (if the package is already installed)
- `brew install --build-bottle acl2s`
- `brew bottle acl2s`

The final command will both produce a bottle archive in the current
directory, and print out some information for the recipe of the form
```
bottle do
  sha256 ...
end
```

Add the `sha256` line to the recipe inside of a `bottle do` block if
one already exists, or copy the whole thing into the recipe if
needed. The `cellar` field must be added to that line, with the cellar
that the bottle was built in. Thus, each architecture/OS with a bottle
should have a line in `bottle do` that looks like `sha256 cellar:
"<Cellar path used when building>", <os/architecture>: <sha256sum
output by brew bottle>`

Then, the bottle archive must be uploaded to a publicly accessible
location. We currently use
`http://invgame-ng.atwalter.com/ubuntu/`. The location should be set
inside the `bottle do` block's `root_url` section, like `root_url
"http://invgame-ng.atwalter.com/ubuntu"`. The bottle archive should be
uploaded to that location, and it should be named
`acl2s-<version>.<architecture>_<os>.bottle.1.tar.gz`. Note that `brew
bottle acl2s` currently creates a file with two dashes at the
beginning instead of one and without the `.1.`, so it must be renamed
to be usable. The `.1.` may not always be required; see[this
link](https://github.com/Homebrew/legacy-homebrew/issues/31812#issuecomment-53137525)
for a description of the naming scheme that Homebrew expects for
bottles.

The bottle archive must then be uploaded to the appropriate
location. After bottles have been generated for all desired
architecture/OS pairs, one can update the recipe with an updated
`bottle do` block containing the information for the newly built
bottles, and push the updates. After the bottle for a particular
architecture/OS combination is uploaded and the recipe is updated to
include it, anyone on that architecture/OS who updates their taps
(e.g. `brew update`) and uses Homebrew to install the `acl2s` package
should not have to rebuild ACL2s, and instead Homebrew should use the
bottle.

## Documentation

`brew help`, `man brew` or check [Homebrew's
documentation](https://docs.brew.sh).
